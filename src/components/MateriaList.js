import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./css/materia.css";
import Navbar from "./Navbar";

const MateriaList = () => {
  const navigate = useNavigate();

  const [materias, setMaterias] = useState([]);

  const loadMateria = async () => {
    const response = await fetch("http://localhost:4000/materia");
    const data = await response.json();
    setMaterias(data);
  };
  //funcion para eliminar una materia
  const handleDelete = async (id_materia) => {
    try {
      await fetch(`http://localhost:4000/materia/${id_materia}`, {
        method: "DELETE",
      });
      alert("Materia eliminada con exito");
      setMaterias(
        materias.filter((materia) => materia.id_materia !== id_materia)
      );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    loadMateria();
  }, []);

  return (
    <>
      <Navbar />
      <div className="lista-materia">
        <div className="crear-materia">
          <button
            className="btn-new-materia"
            onClick={() => navigate("/materia/new")}
          >
            Nueva materia
          </button>
        </div>
        <div className="ejemplo-list">
          <h1>Materias</h1>
          {materias.map((materia) => (
            <div className="card-materia-list" key={materia.id_materia}>
              <div className="card-materia-content">
                
                <div>
                  {" "}
                  <div> {materia.nombre_materia} {" "} </div>
                </div>

                
                <div className="btn-container">
                  <button
                    className="btn-edit"
                    onClick={() =>
                      navigate(`/materia/${materia.id_materia}/edit`)
                    }
                  >
                    Editar
                  </button>
                  <button
                    className="btn-delete"
                    onClick={() => handleDelete(materia.id_materia)}
                  >
                    Eliminar
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
export default MateriaList;
