import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./css/styles.css";
import logo from "../images/FacciLOGO.png";


const Navbar = () => {
  const navigate = useNavigate();
  // const [logged, setLogged] = useState(false);
  // const handleLogin = () => {
  //   setLogged(true)
  //     navigate('/home')
  // }
  // const handleLogout = () => {
  //   setLogged(false);
  //   navigate("/");
  // };

  return (
    <header className="nav">
      <nav className="navbar">
        
          <a href={"/home"}>
            {" "}
            <img className="logo-nav" src={logo} />{" "}
          </a>
          <a className="nav-gest-lab" href={"#"}>
            {" "}
            <h5>GESTIÓN DE LABORATORIOS</h5>{" "}
          </a>

        {/* </div> */}
       <div className="container-opciones" > 

        <ul className="nav-menu">
          <li className="opt-menu"><a href="/reserva"> Reservar Laboratorio</a></li>
          <li className="opt-menu"><a href="/observacion" >Observaciones</a></li>
          <li className="opt-menu"><a>Opciones</a>
            <ul className="menu-opciones">
              <li><a href="/materia" >Materia</a></li>
              <li><a href="/lab">Laboratorio</a></li>
              {/* <li><a>Docente</a></li> */}
              <li><a href="/">Cerrar sesion</a></li>
            </ul>
          </li>
          
        </ul>
       
       </div>
        
      </nav>
    </header>
  );
};

export default Navbar;
