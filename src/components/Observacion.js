import React from "react";
import "./css/observaciones.css";
import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "./Navbar";

const Observacion = () => {

  const [observacion, setObservacion] = useState({
    id_docente: "",
    descripcion_recomendacion: "",
    descripcion_observacion: "",
  });
  const [docentes, setDocentes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(false);
  
  const navigate = useNavigate();
   const params = useParams();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    if (editing){
      await fetch(`http://localhost/observacion/${params.id_observacion}`,{
        method: "PUT",
        headers:{
          "Content-Type": "application/json",
        },
        body: JSON.stringify(observacion)
      });
    } else {
      await fetch("http://localhost:4000/observacion", {
       method: "POST",
       body: JSON.stringify(observacion),
       headers: { "Content-Type": "application/json" },
     });
    }
    setLoading(false);
    if (editing){
      alert("Observacion editada exitosamente")
    } else{
      alert("Observacion enviada exitosamente")
    }
    navigate("/observacion");

    };
    
    
    const handleChange = (e) => {
      setObservacion({ ...observacion, [e.target.name]: e.target.value });
    };
    const loadDocente = async () => {
      const response = await fetch("http://localhost:4000/docente");
      const data = await response.json();
      setDocentes(data);
    };
    useEffect(() => {
      loadDocente();
    }, []);



    return (
      <>
      <Navbar />
      <div className="contenedora_historial">
        <section className="seccion_historial">
          <h1 className="titulo" style={{ textAlign: "center" }}>
            OBSERVACIONES DE LABORATORIOS
          </h1>
          <form onSubmit={handleSubmit}>
            <div className="contenedor_obs">
              <div className="date-observacion">
                <label>Docente:</label>
                <select className="docente-select" name="id_docente" onChange={handleChange}>
                  {docentes.map((docente) => (
                    <option key={docente.id_docente} value={docente.id_docente}>
                      {docente.nombre_docente + " " + docente.apellido_docente}
                    </option>
                  ))}
                </select>
              </div>

              <div className="texto_obs1">
                <label>Observaciones:</label>
                <textarea
                  name="descripcion_observacion"
                  rows={5}
                  cols={40}
                  id="obser"
                  defaultValue={""}
                  onChange={handleChange}
                />
              </div>
              <div className="texto_obs2">
                <label>Recomendaciones:</label>
                <textarea
                  name="descripcion_recomendacion"
                  rows={5}
                  cols={40}
                  id="recom"
                  defaultValue={""}
                  onChange={handleChange}
                />
              </div>
              <div className="boton">

                {/* <button className="botn_obs" id="boton" onclick="validar()">Enviar comentarios</button> */}
                <input
                  type="submit"
                  value="Enviar Observación"
                  className="botn_obs"
                />
              </div>
            </div>
          </form>
        </section>
      </div>
    </>
  );
};

export default Observacion;
