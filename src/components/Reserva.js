import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "./Navbar";
import "./css/reserva.css";

const Reserva = () => {

  //use state
  const [separa, separacion] = useState({
    id_laboratorio: "",
    id_docente: "",
    id_materia: "",
    numero_maquinas_usar: "",
    fecha_reserva: "",
    hora_inicio: "",
    hora_fin: "",
  });
  const [editing, setEditing] = useState (false);
  const [loading, setLoading] = useState (false);
  const params = useParams ();
  const navigate = useNavigate();


  //funcion de captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (editing){
      await fetch(`http://localhost:4000/reserva/${params.id_reservar_laboratorio}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(separa),
      })
    } else {
      
     await fetch("http://localhost:4000/reserva", {
        method: "POST",
        body: JSON.stringify(separa),
        headers: { "Content-Type": "application/json" },
      });
      
  
      // if (data === "reserva no guardada") {
      //   alert("error al hacer la reserva");
      // } else if (data === "reserva guardada") {
      //   alert("reserva realizada con exito");
      //   navigate("/reserva");
      // }
    }
    setLoading(false);
    if (editing){
      alert('Se ha editado la reserva exitosamente')
    } else {
      alert('Se ha reservado el laboratorio exitosamente')
    }
    navigate("/reserva")
  };

  const [labs, setLabs] = useState([]);
  const loadLab = async () => {
    const response = await fetch("http://localhost:4000/lab");
    const data = await response.json();
    setLabs(data);
  };


  useEffect(() => {
    loadLab();
  }, []);


  const [materias, setMateria] = useState([]);
  const loadMateria = async () => {
    const response = await fetch("http://localhost:4000/materia");
    const data = await response.json();
    setMateria(data);
  };


  useEffect(() => {
    loadMateria();
  }, []);


  const [docentes, setDocentes] = useState([]);
  const loadDocente = async () => {
    const response = await fetch("http://localhost:4000/docente");
    const data = await response.json();
    setDocentes(data);
  };


  useEffect(() => {
    loadDocente();
  }, []);



  const handleChange = (e) => {
    separacion({ ...separa, [e.target.name]: e.target.value });
  };

  const loadReserva = async (id_reservar_laboratorio) => {
    const res = await fetch (`http://localhost:4000/reserva/${id_reservar_laboratorio}`);
    const data = await res.json();
    separacion({
      id_laboratorio: data.id_laboratorio,
      id_docente: data.id_docente,
      id_materia: data.id_materia,
      numero_maquinas_usar: data.numero_maquinas_usar,
      fecha_reserva: data.fecha_reserva,
      hora_inicio: data.hora_inicio,
      hora_fin: data.hora_fin,
    });
    setEditing(true);
  };

  useEffect(() => {
    if (params.id_reservar_laboratorio){
      loadReserva(params.id_reservar_laboratorio);
    }
  },[params.id_reservar_laboratorio])
  return (
    <>
      <Navbar/>
      <div className="container-reserva">
        <h1>{editing ? "Editar Reserva" : "Reservar Laboratorio"}</h1>
        <form className="reserva" onSubmit={handleSubmit}>
          <div className="date">
            <select
              className="select-reserva"
              name="id_laboratorio"
              id="Elab"
              onChange={handleChange}
            >
              {labs.map((laboratorio) => (
                <option
                  key={laboratorio.id_laboratorio}
                  value={laboratorio.id_laboratorio}
                >
                  {laboratorio.nombre_laboratorio}
                </option>
              ))}
            </select>
            <spam></spam>
            <label className="name-label">Laboratorio</label>
          </div>

          <div className="date">
            <select
              name="id_docente"
              className="tamaño"
              onChange={handleChange}
            >
              {docentes.map((docente) => (
                <option key={docente.id_docente} value={docente.id_docente}>
                  {docente.nombre_docente + " " + docente.apellido_docente}
                </option>
              ))}
            </select>
            <spam></spam>
            <label>Docente</label>
          </div>

          <div className="date">
            <select
              name="id_materia"
              className="tamaño"
              onChange={handleChange}
            >
              {materias.map((materia) => (
                <option key={materia.id_materia} value={materia.id_materia}>
                  {materia.nombre_materia}
                </option>
              ))}
            </select>
            <spam></spam>
            <label>Materia</label>
          </div>

          <div className="date">
            <input
              type="number"
              required
              name="numero_maquinas_usar"
              min="1"
              max="40"
              onChange={handleChange}
            />
            <spam></spam>
            <label>Numero de maquinas a usar</label>
          </div>
          <div className="date">
            <input type="date" name="fecha_reserva" onChange={handleChange} />
            <spam></spam>
            <label>fecha de reserva</label>
          </div>
          <div className="date">
            <input type="time" name="hora_inicio" onChange={handleChange} />
            <spam></spam>
            <label>Hora inicio</label>
          </div>
          <div className="date">
            <input type="time" name="hora_fin" onChange={handleChange} />
            <spam></spam>
            <label>Hora fin</label>
          </div>
          <input
            className="btn-guardar-reserva"
            type="submit"
            value={editing ? "Actualizar" : "Guardar"}
          />
        </form>
      </div>
    </>
  );
};

export default Reserva;
