import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./css/observaciones.css";
import Navbar from "./Navbar";

const ObservacionList = () => {
  const navigate = useNavigate();
  const [observaciones, setObservaciones] = useState([]);

  const loadObservaciones = async () => {
    const response = await fetch("http://localhost:4000/observacion");
    const data = await response.json();
    setObservaciones(data);
  };
  //funcion para eliminar una laboratorio
  const handleDelete = async (id_observacion) => {
    try {
      await fetch(`http://localhost:4000/observacion/${id_observacion}`, {
        method: "DELETE",
      });
      alert("Observacion eliminada con exito");
      setObservaciones(
        observaciones.filter(
          (observacion) => observacion.id_observacion !== id_observacion
        )
      );
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    loadObservaciones();
  }, []);
  return (
    <>
      <Navbar />

      <div className="lista-observacion">
        <div className="crear-new-observacion">
          <button
            className="btn-new-observacion"
            onClick={() => navigate("/observacion/new")}
          >
            Nueva Observacion
          </button>
        </div>
        <div className="ejemplo-list">
          <h1>Observaciones</h1>
          {observaciones.map((observacion) => (
            <div
              className="card-laboratorio-list"
              key={observacion.id_observacion}
            >
              <div className="card-observacion-content">
                <div>
                  <div>
                    {" "}
                    <strong>Observacion:</strong> {observacion.descripcion_observacion}{" "}
                  </div>
                  <div>
                    {" "}
                    <strong>Recomendacion:</strong>{" "}
                    {observacion.descripcion_recomendacion}{" "}
                  </div>
                  
                </div>
                <div className="btn-container">
                  <button
                    className="btn-edit"
                    onClick={() =>
                      navigate(`/lab/${observacion.id_observacion}/edit`)
                    }
                  >
                    {" "}
                    Editar
                  </button>
                  <button
                    className="btn-delete"
                    onClick={() => handleDelete(observacion.id_observacion)}
                  >
                    Eliminar
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
export default ObservacionList;
